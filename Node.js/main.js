// var fs = require("fs");
// // 阻塞代码实例
// // 读取完后才执行完程序
// var data = fs.readFileSync('input.txt');
// // 非阻塞代码实例
// // 不需要等待文件读取完，这样就可以在读取文件时同时执行接下来的代码，大大提高了程序的性能
// // fs.readFile() 是异步函数用于读取文件
// fs.readFile('input.txt', function (err, data) {
//     if (err) return console.error(err);
//     console.log(data.toString());
// });

// console.log(data.toString());
// console.log("程序执行结束!");
// // 输出全局变量 __filename 的值
// console.log( __filename );
// // 输出全局变量 __dirname 的值
// console.log( __dirname );

// console.info("程序开始执行：");//程序开始执行：
// var counter = 10;
// console.log("计数: %d", counter);//计数: 10
// console.time("获取数据");
// //
// // 执行一些代码
// // 
// console.timeEnd('获取数据');//获取数据: 0ms
// console.info("程序执行完毕。");//程序执行完毕
process.on('exit', function(code) {

  // 以下代码永远不会执行
  setTimeout(function() {
    console.log("该代码不会执行");
  }, 0);
  
  console.log('退出码为:', code);
});
console.log("程序执行结束");