const path = require('path');
// const { AutoWebPlugin } = require('web-webpack-plugin');

module.exports = {
  // JS 执行入口文件
  entry: './index.js',
  // entry: {
  //   index: './pages/index/index.js',// 页面 index.html 的入口文件
  //   login: './pages/login/index.js',// 页面 login.html 的入口文件
  // },
  output: {
    // 把所有依赖的模块合并输出到一个 bundle.js 文件
    filename: 'bundle.js',
    // 输出文件都放到 dist 目录下
    path: path.resolve(__dirname, './dist'),
  },
  devServer: {
    //  contentBase: "./public", //本地服务器所加载的页面所在的目录
    //         historyApiFallback: true, //不跳转
    //         inline: true,
    //         hot: true
    // port: 8080
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
          // use: {
          //     loader: "babel-loader"
          // },
        // // 排除 node_modules 目录下的文件，node_modules 目录下的文件都是采用的 ES5 语法，没必要再通过 Babel 去转换
        exclude: path.resolve(__dirname, 'node_modules'), 
          // exclude: /node_modules/
      },
      {
        test: /\.css/,
        use: ['style-loader', 'css-loader'],
      },
      　{
　　　　　　test: /\.(png|jpg)$/,
　　　　　　loader: 'url-loader?limit=8192'
　　　　}
    ]
  },
  devtool: 'source-map', // 输出 source-map 方便直接调试 ES6 源码

   // 只有在开启监听模式时，watchOptions 才有意义
  // 默认为 false，也就是不开启
  watch: true,
  // 监听模式运行时的参数
  // 在开启监听模式时，才有意义
  watchOptions: {
        // 不监听的文件或文件夹，支持正则匹配
        // 默认为空
        ignored: /node_modules/,
        // 监听到变化发生后会等300ms再去执行动作，防止文件更新太快导致重新编译频率太高
        // 默认为 300ms
        aggregateTimeout: 300,
        // 判断文件是否发生变化是通过不停的去询问系统指定文件有没有变化实现的
        // 默认每秒问 1000 次
        poll: 1000
      },
       plugins: [
        // new webpack.BannerPlugin('版权所有，翻版必究'),
        // new HtmlWebpackPlugin({
        //     template: __dirname + "/app/index.tmpl.html" //new 一个这个插件的实例，并传入相关的参数
        // }),
        // new webpack.optimize.OccurrenceOrderPlugin(),
        // new webpack.optimize.UglifyJsPlugin(),
        // new ExtractTextPlugin("style.css")
    ]
};
