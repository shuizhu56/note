import React from 'react';
import { render } from 'react-dom';
import './main.css';

// import Domponent from  './src/pages/base/base' // 基础
// import Domponent from  './src/pages/jsTest/jsTest' //组件 
// import Domponent from  './src/pages/es6/es6' // 
// import Domponent from  './src/pages/jsPoint/jsPoint' // js知识点 
// import Domponent from  './src/pages/gdMap/gdMap' //   高德地图

import Domponent from  './src/pages/flowerOfLife/flowerOfLife' //   生命之花（echat 的 pie）


 
render(
          <Domponent />,document.getElementById('app')
      );
