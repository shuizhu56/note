import React, {Component} from 'react';
import {render} from 'react-dom';
import Echart from '../../public/components/chart/chart'
import axios from 'axios'//ajax请求
import Mock from 'mockjs';//模拟数据

export default class flowerOfLife extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ajaxData: []
        };
    }

    componentWillMount = () => {
        this.producrData()
        axios.get('../../src/public/data/flowerLife.json')
            .then((res) => {
                this.setState({"ajaxData": res.data})
            })
            .catch((err) => {
                /* ... */
            });
    }
    // Mock模拟数据
    producrData = () =>{
        // 获取 mock.Random 对象  
        const Random = Mock.Random;   
        let productData = [];  
        for (let i = 0; i < 8; i++) {  
            let data = {  
                value:45,  
                name: Random.cname(), // Random.cname() 随机生成一个常见的中文姓名  
                data: [
                    {
                        name:  Random.cname()
                    }
                ]
            }   
            productData.push(data)  
        }  
         
        Mock.mock('../../src/public/data/flowerLife.json', 'get', productData);  
    }

    componentDidMount() {
        console.log('所有的子组件加载后调用')
    }
 
    render() {
        let now = new Date();
        let config = {
            text: now.getMonth() + 1,
            subtext: now.getFullYear()
        }
        return (
            <div>
                <Echart data={this.state.ajaxData} config={config}></Echart>
            </div>
        )
    }
}
