import React, {Component} from 'react'
import {render} from 'react-dom'
import Map from '../../public/components/map/map'

export default class gdMap extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.init()
  }

  init() {}

  render() {
    let mapConfig = {
      boxW: '500px',
      boxH: '300px',
    }
    return (
      <div className='gdMap'>
        <Map config={mapConfig}/>
      </div>
    )
  }
}
 