import React, {Component} from 'react'
import {render} from 'react-dom'

class jsTest extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    test1 = () => {
        for (let i = 0; i < 5; i++) {
            setTimeout(function () {
                console.log(new Date, i); //1s 后在同事5次这个
            }, 1000);
        } 
        console.log(new Date, i); //先这个
    }
    render() {

        return (
            <div>
                 {
                     this.test1()
                 }
            </div>
        )
    }
}

export default jsTest
