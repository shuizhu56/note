export const deepCopy = (obj) =>{ 
    let copy; 
    //   null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // 时间
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // 数组
    if (obj instanceof Array) {
        copy = [];
        for (let i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // 对象
    if (obj instanceof Object) {
        copy = {};
        for (let attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("无法copy, 不支持这个类型");
 
}
 

/*
 *  arr:要排序的数组
 *  key:要排序的字段名
 *  desc:排序方式，升序：true/不传
 *               降序：false
 */
export const sortArr=(arr,key,desc)=>{
  if(desc){//升序
    arr.sort((a, b) => a[key].localeCompare(b[key], 'zh-Hans-CN', {sensitivity: 'accent'}))
    return arr;
  }else{//降序
    arr.sort((b, a) => a[key].localeCompare(b[key], 'zh-Hans-CN', {sensitivity: 'accent'})) 
    return arr;
  }
}

export const sortArrInt=(arr,key,desc)=>{
    if(desc){//升序
      //arr.sort((a, b) => a[key].localeCompare(b[key], 'zh-Hans-CN', {sensitivity: 'accent'}))
      arr.sort(function(a, b){
        return a[key] - b[key];
      });
      return arr;
    }else{//降序
     // arr.sort((b, a) => a[key].localeCompare(b[key], 'zh-Hans-CN', {sensitivity: 'accent'})) 
      arr.sort(function(a, b){
        return b[key] - a[key];
      });
      return arr;
    }
  }
 
      // 获取url中"?"符后的字串
  export const getsearWord=( )=>{ 
        let  url = location.href.split('?')[1]; 
        let  theRequest = new Object(); 
            let  str = url;
            let strs = str.split("&");
            for (let  i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
            } 
        return theRequest;
    }
	 export const runAsync=()=>{//通过及时云用户电话号码获取当前用户信息
      var p = new Promise(function(resolve, reject){
        let __rnWebview = window.__rnWebview;
        if(window.__rnWebview){
            let user = __rnWebview.user;
            T.ajax({
                key: 's_user_info_by_phone',
                //data: {phone: '13685231621'},
                data: {phone: user.username},
                f:'json',
                success: function(result){
                  if(result.data.length!=0){
                    setCookie('u_id',result.data[0].u_id,24);
                    setCookie('uname',result.data[0].uname,24);
                   }
                    resolve(result);
                }
            });
        }else{
          resolve(false);
        }
      });
      return p; 
  }
//设置cookie
export const setCookie = (name, value, remainTime) => {
  let d = new Date();
  d.setTime(d.getTime() + (remainTime * 3600000));//小时单位
  let expires = "expires=" + d.toUTCString();
  document.cookie = name + "=" + value + "; " + expires;
}

//获取cookie
export const getCookie = (cname) => {
  let name = cname + "=";
  let ca = document
      .cookie
      .split(';');
  for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') 
          c = c.substring(1);
      if (c.indexOf(name) != -1) 
          return c.substring(name.length, c.length);
      }
  return "";
}

//清除指定名称的cookie
export const clearCookie = (name) => {
  setCookie(name, "", -1);
}

//清除所有cookie
export const clearAllCookie = () => {
  let keys = document
      .cookie
      .match(/[^ =;]+(?=\=)/g); 
  for (let item of keys) {
      clearCookie(item);
  }
}
