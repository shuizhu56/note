import React ,{Component} from 'react'
import {render} from 'react-dom'
// import ReactEcharts from 'echarts-for-react'
import ReactEchartsCore from 'echarts-for-react/lib/core'; 
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/pie';
import 'echarts/lib/component/tooltip';

class chart extends Component {
    constructor(props){
      super(props)
      this.state = { 
      }
      this.data = [];
      this.config = {
          text: '标题',
         subtext: '副标题',
      }
    }
    // 初始化的 只调用一次
    componentWillMount(props,state){
      this.getConfigFn({data:[],config:{}}) 
    } 
          // /渲染后调用
    componentDidMount = () => { 
        console.log('子组件渲染后调用')
    }  

    // 无论props是否有变化都调用
    // componentWillReceiveProps = (props,state) =>{ 
    //     console.log(props.data)
    // }
    // 返回 return true or false
    // shouldComponentUpdate =(props,state) =>{
    //     console.log(2)
    // } 
    // props变化后调用的 ，会执行render
    componentWillUpdate = (props,state) =>{ 
         this.getConfigFn(props)
    } 
    // componentWillUpdate之后调用的
    componentDidUpdate = () =>{ 
        // console.log(2) 
    }

    getConfigFn = (props,state) =>{ 
        this.data = props.data? Object.assign([],this.data,props.data):this.data;  
        this.config = props.config? Object.assign({},this.config,props.config):this.config;  
    }
// ecart 加载后调用
    onChartReadyCallback = () => {
        console.log('ecart 加载后调用')
    }
    render(){  
      let {text,subtext} = this.config;  
      let option = { 
          title: {
              text: text+'月',
              subtext: subtext+'年',
              x: 'center', //center
              y: 'center', //center
              textStyle: {
                  fontWeight: 'normal',
                  fontSize: 16
              }
          },
          backgroundColor: '#dde',
          series: [
              //内圈
              {
                  name: '内圈',
                  type: 'pie',
                  radius: ['30%', '89%'],
                  center: ['50%', '50%'], //位置 
                  label: {
                      normal: {
                          show: true,
                          position: 'inner', //center 
                          formatter: function(param) {
                              var item = param.data.data;
                              switch (item.length) {
                                  case 0:
                                      return "";
                                      break;
                                  case 1:
                                      return '{a|' + item[0].name + '}';
                                      break;
                                  case 2:
                                      return [
                                          '{a|' + item[0].name + '}',
                                          '{b|' + item[1].name + '}'
                                      ].join('\n')
                                      break;
                                  case 3:
                                      return [
                                          '{a|' + item[0].name + '}',
                                          '{b|' + item[1].name + '}',
                                          '{c|' + item[2].name + '}'
                                      ].join('\n')
                                      break;
                              }
                          },
                          rich: {
                              a: {
                                  color: 'pink', 
                              },
                              b: {
                                  color: 'yellow'
                              },
                              c: {
                                  color: 'yellow'
                              }
                          },
                          textStyle: {
                              color: '#fff',
                          }
                      },
                  },
                  labelLine: {
                      normal: {
                          show: false
                      }
                  },

                  data:  this.data
              }, //外圈
              {
                  name: '外圈',
                  type: 'pie',
                  radius: ['90%', '100%'],
                  label: {
                      normal: {
                          show: true, 
                          formatter: function(param) {
                              return param.name;
                          },
                          textStyle: {
                              color: '#fff',
                              fontWeight: 'bold',
                              fontSize: 14
                          }
                      },
                  }, 
                  data: this.data
              }

          ]
      };

      return(
        <div>
          <ReactEchartsCore 
            echarts={echarts}
            option ={ option }   
            lazyUpdate={true} 
            onChartReady={this.onChartReadyCallback}
             />
        </div>
      )
    }
}

export default chart 
