import React, {Component} from 'react';
import {render} from 'react-dom';
import L from 'leaflet';
import './leaflet.css';
import './map.css';
import tool from '../../js/tool';
// import '//at.alicdn.com/t/font_636057_qqj9nmssmza7nwmi.css'

// import markerImg  from './images/defaultIcon.png';

class map extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.Amap = null;
    this.config = {
      boxW: 100,
      boxH: 100,
      centerXY: [
        37.9, 115.2
      ],//地图中心
      scale: 12,//缩放级别
      mapType: 'gd',
      markerStyle:{
          className    : 'leaflet_point',                         // dom className
          iconUrl      : './src/public/images/defaultIcon.png',  // 点图片路径
          iconSize     : [40, 40],                                // 点大小
          iconAnchor   : [20, 40],                            // 点偏移 1/2 iconSize 为居中 
      }
    }
    this.mapType = {
      gd: 'http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x=' +
          '{x}&y={y}&z={z}',//高德
      yt:'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      gg:'http://{s}.tiles.mapbox.com/v3/MapID/{z}/{x}/{y}.png',//谷歌
    }
  };



  componentWillMount() {
    // this.config = this.props.config? [...this.props.config,this.config ]
    this.config = this.props.config
      ? Object.assign({}, this.config, this.props.config)
      : this.config
  }

  componentDidMount() {
    this.init()
  }

  init() {
    let mapOptions = {
      attributionControl: false
    }
    this.Amap = L
      .map('map', mapOptions)
      .setView(this.config.centerXY, this.config.scale);
    L.tileLayer(this.mapType[this.config.mapType], {subdomains: "1234"}).addTo(this.Amap);
    // this.setState({})

    this.markerPoint()
    this.circleArea()
    this.circleArea()
    
  }
// 添加标注
  markerPoint(data){
    data = [{
      x:38,
      y:115,
      popup:'展示信息'
    }]
    data.map((item,i) => {
      L.marker([item.x, item.y],
      {icon:L.icon(this.config.markerStyle)}
      ).addTo(this.Amap)
        .bindPopup(item.popup)
        .openPopup() 
    })
  }
  // 添加圆形
  circleArea(){
       L.circle([38, 116], 550, {
        color: 'red',
            fillColor: '#f03',
        fillOpacity: 0.5
        }).addTo(this.Amap);
  }
  // 添加多边形
    polygon(){
       L.polygon([
         [39, 118], 
         [39, 119]
         [40, 118]
        ]).addTo(this.Amap);
  }
 
  render() {
    return (
      <div className='map'>
        <div
          id="map"
          style={{
          width: this.config.boxW,
          height: this.config.boxH
        }}></div>
      </div>
    )
  }
}

export default map