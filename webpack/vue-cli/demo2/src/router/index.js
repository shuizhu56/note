import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import other from '@/pages/other/other'
import about from '@/pages/about/about'
import home from '@/pages/home/home'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/other',
        name: 'other',
        component: other
    }, {
        path: '/about',
        name: 'about',
        component: about
    }, {
        path: '/',
        name: 'home',
        component: home
    }]
})