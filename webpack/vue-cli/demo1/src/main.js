 
import ElementUI from 'element-ui' 
// import  {Container,Header,Main,Collapse,CollapseItem,Footer,Row,Col,Menu,MenuItem, Aside} from 'element-ui' 
 

// import 'element-ui/lib/theme-chalk/index.css' //放在import App from './App';之前

import Vue from 'vue'
import App from './App'
import router from './router' 


Vue.use(ElementUI)  
// Vue.use(Container)
// Vue.use(Header)
// Vue.use(Main)
// Vue.use(Aside)
// Vue.use(Footer)
// Vue.use(Row)
// Vue.use(Col) 
// Vue.use(Menu)
// Vue.use(MenuItem)
// Vue.use(Collapse)
// Vue.use(CollapseItem)


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
