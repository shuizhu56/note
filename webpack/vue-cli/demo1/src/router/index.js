import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'

// 组件
// import btn from '@/components/btn'//新添加，之后在下方的component: btn 才会生效
// import layout from '@/components/layout' 
// import table from '@/components/table' 
// import head from '@/components/head'
// import foot from '@/components/foot'


// 页面集成
import main from '@/pages/main' 
import app from '@/pages/app' 
import share from '@/pages/share' 
import animate from '@/pages/animate' 
import es6 from '@/pages/es6' 
import monthPlan from '@/pages/monthPlan' 
import jsbase from '@/pages/jsbase' 
import vueBase from '@/pages/vuex' 

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/HelloWorld',
    //   name: 'HelloWorld',  
    //   component: HelloWorld
    // },
    {
      path: '/',
      name: 'main',  
      component: main
    },
    {
      path: '/app',
      name: 'app',  
      component: app
    },
    {
      path: '/share',
      name: 'share',  
      component: share
    },  
    {
      path: '/es6',
      name: 'es6',  
      component: es6,
      meta: {
        keepAlive: true // 缓存
      }
    },
    {
      path: '/animate',
      name: 'animate',  
      component: animate,
      meta: {
        keepAlive: false // 不需要缓存
      }
    },
    {
      path: '/monthPlan',
      name: 'monthPlan',  
      component: monthPlan
    },
    {
      path: '/jsbase',
      name: 'jsbase',  
      component: jsbase,
      meta: {
        keepAlive: true // 缓存
      }
    },
    {
      path: '/vueBase',
      name: 'vueBase',  
      component: vueBase,
      meta: {
        keepAlive: true // 缓存
      }
    },
    // {
    //   path: '/head',
    //   name: 'head',  
    //   component: head
    // },
    // {
    //   path: '/btn',
    //   name: 'btn',
    //   component: btn
    // },{
    //   path: "/layout",
    //   name: "layout",
    //   component: layout
    // },{
    //   path: "/table",
    //   name: "table",
    //   component: table
    // },{
    //   path: "/foot",
    //   name: "foot",
    //   component: foot
    // }
  ]
})
