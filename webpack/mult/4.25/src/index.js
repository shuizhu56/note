import _ from 'lodash';
// import { cube } from './math.js';
import Print from './print.js';
// import './styles.css';


// async function getComponent() {
function component() {
    var element = document.createElement('pre')
    var button = document.createElement('button');
    var br = document.createElement('br');
    button.innerHTML = 'Click me and look at the console!';
    // element.innerHTML = _.join(['Hello', 'webpack'], ' ');

    element.innerHTML = [
        'Hello webpack!',
        '5 cubed is equal to ' +
        cube(5)
    ].join('\n\n');
    return element;
}

let element = component();
document.body.appendChild(element);

// if (module.hot) {
//     module.hot.accept('./print.js', function() {
//         console.log('Accepting the updated printMe module!');
//         document.body.removeChild(element);
//         element = component(); // Re-render the "component" to update the click handler
//         document.body.appendChild(element);
//     })
// }