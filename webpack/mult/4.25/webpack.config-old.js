const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    // entry: './src/index.js',
    entry: {
        index: './src/index.js',
        another: './src/another-module.js'
            // print: './src/print.js'
    },
    devtool: 'inline-source-map', //仅用于开发环境
    mode: "development", //production 生产压缩 \development 开发
    output: {
        filename: 'main.js',
        // filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }, {}]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(), //模块热替换
        new HtmlWebpackPlugin({
            title: 'Output Management'
        }),

    ],
    optimization: {
        // 将公共的依赖模块提取到已有的入口 chunk 中，或者提取到一个新生成的 chunk。
        splitChunks: {
            chunks: 'all'
        }
    },
    // 将 dist 目录下的文件，作为可访问文件。
    devServer: {
        contentBase: './dist',
        hot: true
    },
}