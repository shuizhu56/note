const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin'); //清理 /dist 文件夹

const merge = require('webpack-merge');

// const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'production',
    // devtool: 'source-map', //仅用于生产环境 
    plugins: [
        new CleanWebpackPlugin(['dist']),

        // new UglifyJSPlugin({
        //     sourceMap: true
        // }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ]
});