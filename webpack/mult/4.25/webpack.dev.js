const merge = require('webpack-merge');
const common = require('./webpack.common.js');
module.exports = merge(common, {
    mode: 'development',
    // devtool: 'inline-source-map', //仅用于开发环境
    // 将 dist 目录下的文件，作为可访问文件。
    devServer: {
        contentBase: './dist',
        hot: true
    }
});