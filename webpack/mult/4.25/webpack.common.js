const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// import pageArr from './src/pageArr';
// const pageArr = require('./src/pageArr');



module.exports = {
    //  entry :  './src/pages/home/home.js',
    entry: {
        home: './src/pages/home/home.js',
        // other: './src/pages/other/other.js', 
        vendor: [
                'lodash'
            ]
            // another: './src/another-module.js'
    },
        output: {
        filename: '[name]/[name].js', //'[name].[chunkhash].js',
        // chunkFilename: '[name].[chunkhash].js', //
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        // [...pageArr], 
        new HtmlWebpackPlugin({
            title: 'Production',
            filename: __dirname + '/dist/home.html', //输出路径和文件名
            // inject: 'head',
            template: __dirname + "/src/pages/home/home.html",
            // template: 'html-withimg-loader!'+__dirname + "/src/pages/home/home.html",
            chunks: ['home','other'],
            // inlineSource: '.(js|css)$' 
        }),
        // new HtmlWebpackPlugin({
        //     title: 'Production',
        //     filename: __dirname + '/dist/other.html', //输出路径和文件名
        //     inject: 'head',
        //     template: 'html-withimg-loader!'+__dirname + "/src/pages/other/other.html",
        //     chunks: ['other'],
        //     // inlineSource: '.(js|css)$' 
        // }),

        // new webpack.HashedModuleIdsPlugin(), 
        // new CommonsChunkPlugin({
        //     name: 'manifest',
        //     minChunks: Infinity
        //   })
 
    ],
    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }, 
          {
                // 对js文件使用loader
                test: /\.js$/,
                use: 'babel-loader'
            },
        // {
        //     // test: /\.(tpl|ejs)$/i,
        //     test: /\.tpl$/,            
        //     use:[ 'ejs-loader']
        //     // use:[ 'html-withimg-loader']
        // },
        {
        test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                    attrs: [':data-src']
                    }
                }
        },
         { test: /\.jpg$/, use: [ "file-loader" ] },
]
    },

    optimization: {
        // 将公共的依赖模块提取到已有的入口 chunk 中，或者提取到一个新生成的 chunk。
        // splitChunks: {
        //     chunks: 'all'
        // },
        runtimeChunk: 'single', //将模块分离到单独的文件中
        splitChunks: {
            // //避免vendor 的 hash变化
            cacheGroups: {
                // 第三方库(library)（例如 lodash 或 react）提取到单独的 vendor chunk 文件
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        }
    },
}