# GULP
----------
## gulp.js 是一种基于流的，代码优于配置的新一代构建工具。

## gulp有五个方法：src、dest、task、run、watch
> * src和dest：指定源文件和处理后文件的路径
> * watch：用来监听文件的变化
> * task：指定任务
> * run：执行任务

# 项目目录结构
project(项目名称)
|–.git 通过git管理项目会生成这个文件夹
|–node_modules 组件目录
|–dist 发布环境
    |–css 样式文件(style.css style.min.css)
    |–images 图片文件(压缩图片)
    |–js js文件(main.js main.min.js)
    |–index.html 静态文件(压缩html)
|–src 生产环境
    |–sass sass文件
    |–images 图片文件
    |–js js文件
    |–index.html 静态文件
|–.jshintrc jshint配置文件
|–gulpfile.js gulp任务文件

# 安装
Gulp是基于 Node.js的，故要首先安装 Node.js，然后在global环境和项目文件中都install gulp

```npm install gulp -g   (global环境)```

```npm install gulp --save-dev (项目环境) ```

## 在项目中install需要的gulp插件，一般只压缩的话需要
```npm install gulp-minify-css gulp-concat gulp-uglify gulp-rename del --save-dev```

编译Sass ：gulp-ruby-sass

gulp-ruby-sass是调用sass，所以需要ruby环境，需要生成临时目录和临时文件

gulp-sass是调用node-sass,有node.js环境就够了，编译过程不需要临时目录和文件，直接通过buffer内容转换。

npm install gulp-ruby-sass --save-dev

Autoprefixer ：gulp-autoprefixer

缩小化(minify)CSS ：(gulp-minify-css)

检查js ：gulp-jshint

拼接文件： gulp-concat

压缩js：gulp-uglify

图片压缩 (gulp-imagemin)：利用快取来避免重複压缩已经压缩好的图片；

重命名文件：gulp-rename

压缩html：gulp-htmlmin

清空文件夹(清理档案)：gulp-clean

即时重整 或者 服务器控制客户端同步刷新（需配合chrome插件LiveReload及tiny-lr）:gulp-livereload

图片快取，只有更改过得图片会进行压缩 (gulp-cache)

更动通知 ：gulp-notify

`$ npm install gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-clean gulp-notify gulp-rename gulp-livereload gulp-cache --save-dev`

更多插件可以在这个链接中找到 http://gratimax.net/search-gulp-plugins/

安装必要的插件，并纪录于package.json内的devDependencies物件

### 在项目的根目录新建gulpfile.js，require需要的module
`var gulp = require('gulp'),`

 minifycss = require('gulp-minify-css'),

concat = require('gulp-concat'),

uglify = require('gulp-uglify'),

rename = require('gulp-rename'),

del = require('del');

### Gulpfile.js例子2：

var gulp = require('gulp');

var jshint = require('gulp-jshint');

var paths = {
  scripts: 'js/**/*.js',
};

gulp.task('lint', function() {
  return gulp.src(paths.scripts)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

####执行：gulp lint

### Gulpfile.js例子3：

var gulp = require('gulp'), 
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),//
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload');


###压缩css

gulp.task('minifycss', function() {
    return gulp.src('src/*.css')      //压缩的文件
        .pipe(gulp.dest('minified/css'))   //输出文件夹
        .pipe(minifycss());   //执行压缩
});

Ps：
这个gulp.src用来定义一个或多个来源档案。允许使用glob样式，例如/**/*.css 比对多个符合的档案。传回的串流(stream)让它成为非同步机制，所以在我们收到完成通知之前，确保该任务已经全部完成。
使用pipe()来串流来源档案到某个插件。插件的选项通常在它们各自的Github页面中可以找到。
这个gulp.dest()是用来设定目的路径。一个任务可以有多个目的地，一个用来输出扩展的版本，一个用来输出缩小化的版本

### 压缩js

gulp.task('minifyjs', function() {
    return gulp.src('src/*.js')
        .pipe(concat('main.js'))    //合并所有js到main.js
        .pipe(gulp.dest('minified/js'))    //输出main.js到文件夹
        .pipe(rename({suffix: '.min'}))   //rename压缩后的文件名
        .pipe(uglify())    //压缩
        .pipe(gulp.dest('minified/js'));  //输出
});
执行压缩前，先删除文件夹里的内容
gulp.task('clean', function(cb) {
    del(['minified/css', 'minified/js'], cb)
});

### JSHint，拼接及缩小化JavaScript

gulp.task('scripts', function() { 
  return gulp.src('src/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

Ps：需要指定JSHint一个reporter

### 编译Sass，Autoprefix及缩小化
gulp.task('styles', function() { 

return gulp.src('src/styles/main.scss')

.pipe(sass({ style: 'expanded' }))//编译Sass

.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))//通过Autoprefixer，添加前缀 

.pipe(gulp.dest('dist/assets/css'))//保存未压缩文件到我们指定的目录下面

.pipe(rename({suffix: '.min'}))//产生一个缩小化的.min版本

.pipe(minifycss())//压缩样式文件，自动重新整理页面

.pipe(gulp.dest('dist/assets/css'))//输出压缩文件到指定目录

.pipe(notify({ message: 'Styles task complete' }));//通知任务已经完成

});

####默认命令，在cmd中输入gulp后，执行的就是这个命令
gulp.task('default', ['clean'], function() {
    gulp.start('minifycss', 'minifyjs');
});

然后只要cmd中执行，gulp即可


### LiveReload配置

1、安装Chrome LiveReload

2、通过npm安装http-server ，快速建立http服务

npm install http-server -g

3、通过cd找到发布环境目录dist

4、运行http-server，默认端口是8080

5、访问路径http:127.0.0.1:8080

6、再打开一个cmd，通过cd找到项目路径执行gulp，清空发布环境并初始化

7、执行监控 gulp watch

8、点击chrome上的LiveReload插件，空心变成实心即关联上，你可以修改css、js、html即时会显示到页面中。
