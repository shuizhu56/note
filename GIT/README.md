## 本地文件，未提交到git上面的，现在要提交git：
### 打开当前文件夹， 先执行 git init 进行仓库的初始化；
## 本地无文件，而是从git上面clone下来的 ，执行 git clone + 路径
## 文件修改要进行提交：
### git add . //提交所有修改的文件到暂存区
### git add + 具体文件名 //只提交这个文件
### git commit -m '***' //添加的注释
### git pull //同步服务器的文件
### git push origin master //提交到服务器上面
## 其他：
### 查看提交历史记录
#### git log 
### 查看配置信息
#### git config --list 
### 检查当前文件状态
####  git status
### 远程服务器：
#### 查看远程仓库：git remote -v
#### 添加远程仓库：git remote add [name] [url]
#### 删除远程仓库：git remote rm [name]
#### 修改远程仓库：git remote set-url --push[name][newUrl]
#### 拉取远程仓库：git pull [remoteName] [localBranchName]
#### 推送远程仓库：git push [remoteName] [localBranchName]

### 分支
#### 查看分支：git branch 
#### 创建分支：git branch name 
#### 切换分支：git checkout name 
#### 创建+切换分支：git checkout -b name 
#### 合并某分支到当前分支：git merge name 
#### 删除分支：git branch -d name

### 如果希望用代码库中的文件完全覆盖本地工作版本. 方法如下:
#### git reset --hard
#### git pull



# 参考文档
## 手把手教你使用Git
## http://blog.jobbole.com/78960/

## window下配置SSH连接GitHub、GitHub配置ssh key
## http://jingyan.baidu.com/article/a65957f4e91ccf24e77f9b11.html

## 使用TortoiseGit对Git版本进行分支操作
## http://doophp.sinaapp.com/archives/other/branches-options-with-tortoisegit.html

## 使用 GitHub, Jekyll 打造自己的独立博客
## http://www.tuicool.com/articles/BVVBvu